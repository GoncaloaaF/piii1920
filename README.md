# Info orais #

devido a um imprevisto vou me atrasar uns 10 a 15 min pevo desculpa




# quem enviou o projecto por git, necessito que reenviem os convites #


# Programação III / Programação de Computadores III  1920 #

## Links uteis ##

[Tuturial para instalar VM](https://www.youtube.com/watch?v=lHCLFK2bDPM&t=186s)

[E-Book de git](https://git-scm.com/book/en/v2)






## informações Importantes ##

[Notas](https://bitbucket.org/GoncaloaaF/piii1920/downloads/notasv3.pdf)


As notas que serão lançadas (assim que tiver as pautas lanço) não devem variar muito destas

Considerei os trabalhos que recebi independentemente da nota do exame teorico.

Caso a nota do trabalho seja inferior a do exame não contabilizei o trabalho.


Qualque qestão disponham.


Duas questoes tinham duas respostas que se poderia considerar corretas, ambas as respostas tiveram cotação.


## Slides ##


[Swift 101](https://bitbucket.org/GoncaloaaF/piii1920/downloads/Swift_101.pdf)

[Classes](https://bitbucket.org/GoncaloaaF/piii1920/downloads/swift_class.pdf)

[iOS Technologies](https://bitbucket.org/GoncaloaaF/piii1920/downloads/iOS_Technologies_.pdf)

[Anatomy of an app](https://bitbucket.org/GoncaloaaF/piii1920/downloads/anatomy_of_an_app.pdf)


[Preguntas de exemplo](https://bitbucket.org/GoncaloaaF/piii1920/downloads/pregExemplo.pdf) <- no exame apenas havera 4 hipóteses de resposta, publico a soluçao mais perto do exame

[Solução das Preguntas de exemplo](https://bitbucket.org/GoncaloaaF/piii1920/downloads/pregExemplo_sol.pdf) <- as respostas estão assinaladas a amarelo, duas comentadas, qualquer questão disponham 

### Exemplos  ###

[Semana 2](https://bitbucket.org/GoncaloaaF/piii1920/downloads/Semana2.playground.zip)

[Classes](https://bitbucket.org/GoncaloaaF/piii1920/src/master/Exemplos/demoClasses/)

[Propriedades](https://bitbucket.org/GoncaloaaF/piii1920/src/master/Exemplos/demo_porp.swift)

[Intro Xcode](https://bitbucket.org/GoncaloaaF/piii1920/src/master/Exemplos/intro_xcode/)

[Tabelas](https://bitbucket.org/GoncaloaaF/piii1920/src/master/Exemplos/Tabelas_PCIIIPL_v2/)

[Tabelas - Custom Cell](https://bitbucket.org/GoncaloaaF/piii1920/src/master/Exemplos/tabelas_customCell/)

[Teste Tipo](https://bitbucket.org/GoncaloaaF/piii1920/src/master/Exemplos/testeTipo_final/)




## Fichas de trabalho ##

[ficha 0 - > revisões](https://bitbucket.org/GoncaloaaF/piii1920/downloads/ficha_0.pdf)

[Ficha 1 - intrudução](https://bitbucket.org/GoncaloaaF/piii1920/downloads/ISTEC_ficha_funcs.pdf)

[Ficha 2 - collections](https://bitbucket.org/GoncaloaaF/piii1920/downloads/ISTEC_ficha_collections.pdf)

[Ficha 3 - clss](https://bitbucket.org/GoncaloaaF/piii1920/downloads/ficha_classes.pdf)


### Solução Fichas de trabalho ###


[Sol Ficha 1](https://bitbucket.org/GoncaloaaF/piii1920/src/master/Exemplos/SolFichas/ficha1.playground/)

[Sol Ficha 2](https://bitbucket.org/GoncaloaaF/piii1920/src/master/Exemplos/SolFichas/ficha2.playground/)






