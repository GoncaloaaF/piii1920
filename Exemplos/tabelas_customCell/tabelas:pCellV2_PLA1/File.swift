//
//  File.swift
//  tabelas:pCellV2_PLA1
//
//  Created by Gonçalo Feliciano on 13/01/2020.
//  Copyright © 2020 Gonçalo Feliciano. All rights reserved.
//

import UIKit

class Comida{
    
    
    var nome:String
    var foto:UIImage
    
    
    init(nome:String, foto: String) {
        self.nome = nome
        self.foto = UIImage(named: foto) ?? UIImage(named: "noImg")!
    }
}
